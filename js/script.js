
    fetchUser(); //This function will load all data on web page when page load
    function fetchUser() // This function will fetch data from table and display under <div id="result">
    {
        var action = "Load";
        $.ajax({
            url: "action.php",
            method: "POST",
            data: {action: action},
            success: function (data) {
                $('#result').html(data);
            }
        });
    }

    //This JQuery code will Reset value of Modal item when modal will load for create new records
    $('#modal_button').click(function () {
        $('#customerModal').modal('show');
        $('#name').val('');
        $('#age').val('');
        $('.modal-title').text("Create New Records");
        $('#action').val('Create');
    });

    //This JQuery code is for Click on Modal action button for Create new records or Update existing records.
    $('#action').click(function () {
        var name = $('#name').val();
        var age = $('#age').val();
        var id = $('#user_id').val();
        var action = $('#action').val();
        if (name != '' && age != '')
        {
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {name: name, age: age, id: id, action: action},
                success: function (data) {
                    alert(data);
                    $('#customerModal').modal('hide');
                    fetchUser();
                }
            });
        } else {
            alert("Both Fields are Required");
        }
    });

    //This JQuery code is for user customer data.
    $(document).on('click', '.update', function () {
        var id = $(this).attr("id"); //This code will fetch any customer id from attribute id with help of attr() JQuery method
        var action = "Select";   //We have define action variable value is equal to select
        $.ajax({
            url: "action.php",   //Request send to "action.php page"
            method: "POST",    //Using of Post method for send data
            data: {id: id, action: action},//Send data to server
            dataType: "json",   //Here we have define json data type, so server will send data in json format.
            success: function (data) {
                $('#customerModal').modal('show');   //It will display modal on webpage
                $('.modal-title').text("Update Records"); //This code will change this class text to Update records
                $('#action').val("Update");     //This code will change Button value to Update
                $('#user_id').val(id);     //It will define value of id variable to this customer id hidden field
                $('#name').val(data.name);  //It will assign value to modal first name texbox
                $('#age').val(data.age);  //It will assign value of modal last name textbox
            }
        });
    });


    //This JQuery code is for show one user customer data.
    $(document).on('click', '.Select', function () {
        var id = $(this).attr("id");
        var action = "Select";
        $.ajax({
            url: "action.php",
            method: "POST",
            data: {id: id, action: action},
            dataType: "json",
            success: function (data) {
                $('#customerModalShow').modal('show');
                $('#user_id').val(id);
                $('#name_show').val(data.name);
                $('#age_show').val(data.age);
            }
        });
    });

    //This JQuery code is for Delete customer data. If we have click on any customer row delete button then this code will execute
    $(document).on('click', '.delete', function () {
        var id = $(this).attr("id");
        if (confirm("Are you sure you want to remove this data?"))
        {
            var action = "Delete";
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {id: id, action: action},
                success: function (data) {
                    fetchUser();
                    alert(data);
                }
            })
        } else
        {
            return false;
        }
    });
