<html>
<head>
    <title>AMB</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #f1f1f1;
        }

        .box {
            width: 1270px;
            padding: 20px;
            background-color: #fff;
            border: 1px solid #ccc;
            border-radius: 5px;
            margin-top: 100px;
        }
    </style>
</head>
<body>
<div class="container box">
    <h1 align="center">PHP Task</h1>
    <br/>
    <div align="right">
        <button type="button" id="modal_button" class="btn btn-info">Create User</button>
    </div>
    <br/>
    <div id="result" class="table-responsive">

    </div>
</div>
</body>
</html>

<div id="customerModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create New User</h4>
            </div>
            <div class="modal-body">
                <label>Name</label>
                <input type="text" name="name" id="name" class="form-control"/>
                <br/>
                <label>Age</label>
                <input type="text" name="age" id="age" class="form-control"/>
                <br/>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="user_id" id="user_id"/>
                <input type="submit" name="action" id="action" class="btn btn-success"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div id="customerModalShow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Show User</h4>
            </div>
            <div class="modal-body">
                <label>Name</label>
                <input type="text" name="name" readonly id="name_show" class="form-control"/>
                <br/>
                <label>Age</label>
                <input type="text" name="age" id="age_show" readonly class="form-control"/>
                <br/>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="user_id" id="user_id"/>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="js/script.js"></script>
