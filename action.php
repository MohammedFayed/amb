<?php

require 'config/Database.php';

if (isset($_POST["action"])) //Check value of $_POST["action"] variable value is set to not
{
    //For Load All Data
    if ($_POST["action"] == "Load") {
        $statement = $connection->prepare("SELECT * FROM users ORDER BY id DESC");
        $statement->execute();
        $result = $statement->fetchAll();
        $output = '';
        $output .= '
   <table class="table table-bordered">
    <tr>
     <th width="40%">Name</th>
     <th width="40%">Age</th>
     <th width="10%">Show</th>
     <th width="10%">Update</th>
     <th width="10%">Delete</th>
    </tr>
  ';
        if ($statement->rowCount() > 0) {
            foreach ($result as $row) {
                $output .= '
    <tr>
     <td>' . $row["name"] . '</td>
     <td>' . $row["age"] . '</td>
     <td><button type="button" id="' . $row["id"] . '" class="btn btn-warning btn-xs Select">Show</button></td>
     <td><button type="button" id="' . $row["id"] . '" class="btn btn-warning btn-xs update">Update</button></td>
     <td><button type="button" id="' . $row["id"] . '" class="btn btn-danger btn-xs delete">Delete</button></td>
    </tr>
    ';
            }
        } else {
            $output .= '
    <tr>
     <td align="center">Data not Found</td>
    </tr>
   ';
        }
        $output .= '</table>';
        echo $output;
    }

    //This code for Create new Records
    if ($_POST["action"] == "Create") {


        $name =$_POST["name"];
        $age =$_POST["age"];
        $query = $connection->prepare("INSERT INTO users(name, age) VALUES (:name,:age)");
        $query->bindParam("name", $name, PDO::PARAM_STR);
        $query->bindParam("age", $age, PDO::PARAM_STR);
        $result=  $query->execute();
        if (!empty($result)) {
            echo 'Data Inserted';
        }
    }

    //This Code is for fetch single customer data for display on Modal
    if ($_POST["action"] == "Select") {
        $output = array();
        $statement = $connection->prepare("SELECT * FROM users WHERE id = '" . $_POST["id"] . "' LIMIT 1");
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $row) {
            $output["name"] = $row["name"];
            $output["age"] = $row["age"];
        }
        echo json_encode($output);
    }

    //This code for update  Records
    if ($_POST["action"] == "Update") {
        $statement = $connection->prepare("UPDATE users SET name = :name, age = :age WHERE id = :id");
        $result = $statement->execute(
            array(
                ':name' => $_POST["name"],
                ':age' => $_POST["age"],
                ':id' => $_POST["id"]
            )
        );
        if (!empty($result)) {
            echo 'Data Updated';
        }
    }

    //This code for delete  Records
    if ($_POST["action"] == "Delete") {
        $statement = $connection->prepare("DELETE FROM users WHERE id = :id");
        $result = $statement->execute(
            array(
                ':id' => $_POST["id"]
            )
        );
        if (!empty($result)) {
            echo 'Data Deleted';
        }
    }

}


 